package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.models.Istrazivanje
import org.json.JSONArray
import org.json.JSONObject
import org.json.JSONTokener
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

object IstrazivanjeIGrupaRepository {
    fun getIstrazivanjeByGodina(godina:Int) : List<Istrazivanje> {
        val lista = mutableListOf<Istrazivanje>()
        for(i in getIstrazivanja()){
            if(i.godina == godina)
                lista.add(i)
        }
        return lista
    }

    fun getIstrazivanja():List<Istrazivanje>{
        val list = mutableListOf<Istrazivanje>()
        val poz = listOf(1,2)
        poz.forEach { a ->
            try {
                val url = URL(ApiConfig.baseURL + "/istrazivanje?offset=$a")
                (url.openConnection() as? HttpURLConnection)?.run {
                    BufferedReader(InputStreamReader(inputStream)).use {
                        val response = StringBuffer()

                        var inputLine = it.readLine()
                        while (inputLine != null) {
                            response.append(inputLine)
                            inputLine = it.readLine()
                        }
                        //println("Response : $response")

                        val jsonArray = JSONTokener(response.toString()).nextValue() as JSONArray
                        for (i in 0 until jsonArray.length()) {
                            // ID
                            val id = jsonArray.getJSONObject(i).getInt("id")
                            val naziv = jsonArray.getJSONObject(i).getString("naziv")
                            val godina = jsonArray.getJSONObject(i).getInt("godina")

                            list.add(Istrazivanje(id, naziv, godina))
                        }
                    }
                }
            } catch (e: Exception) {
                println("Greska: $e")
            }
        }
        return list
    }

    fun getIstrazivanja(offset:Int):List<Istrazivanje>{
        val list = mutableListOf<Istrazivanje>()
        try {
            val url = URL (ApiConfig.baseURL+"/istrazivanje?offset=$offset")
            (url.openConnection() as? HttpURLConnection)?.run {
                BufferedReader(InputStreamReader(inputStream)).use {
                    val response = StringBuffer()

                    var inputLine = it.readLine()
                    while (inputLine != null) {
                        response.append(inputLine)
                        inputLine = it.readLine()
                    }
                    //println("Response : $response")

                    val jsonArray = JSONTokener(response.toString()).nextValue() as JSONArray
                    for (i in 0 until jsonArray.length()) {
                        // ID
                        val id = jsonArray.getJSONObject(i).getInt("id")
                        val naziv = jsonArray.getJSONObject(i).getString("naziv")
                        val godina = jsonArray.getJSONObject(i).getInt("godina")

                        list.add(Istrazivanje(id, naziv, godina))
                    }
                    return list
                }
            }
        }catch (e: Exception){
            println("Greska: $e")
        }
        return list
    }

    fun getIstrazivanjeById(idIstrazivanja: Int): Istrazivanje? {
        try {
            val url = URL (ApiConfig.baseURL+"/istrazivanje/$idIstrazivanja")
            (url.openConnection() as? HttpURLConnection)?.run {
                BufferedReader(InputStreamReader(inputStream)).use {
                    val response = StringBuffer()

                    var inputLine = it.readLine()
                    while (inputLine != null) {
                        response.append(inputLine)
                        inputLine = it.readLine()
                    }
                    //println("Response : $response")

                    val jsonObject = JSONTokener(response.toString()).nextValue() as JSONObject
                    val id = jsonObject.getInt("id")
                    val naziv = jsonObject.getString("naziv")
                    val godina = jsonObject.getInt("godina")

                    return Istrazivanje(id, naziv, godina)
                }
            }
        }catch (e: Exception){
            println("Greska: $e")
        }
        return null
    }

    fun getGrupe():List<Grupa>{
        val grupe = mutableListOf<Grupa>()
        try {
            val url = URL (ApiConfig.baseURL+"/grupa")
            (url.openConnection() as? HttpURLConnection)?.run {
                BufferedReader(InputStreamReader(inputStream)).use {
                    val response = StringBuffer()

                    var inputLine = it.readLine()
                    while (inputLine != null) {
                        response.append(inputLine)
                        inputLine = it.readLine()
                    }
                    //println("Response : $response")

                    val jsonArray = JSONTokener(response.toString()).nextValue() as JSONArray
                    for (i in 0 until jsonArray.length()) {
                        // ID
                        val id = jsonArray.getJSONObject(i).getInt("id")
                        val naziv = jsonArray.getJSONObject(i).getString("naziv")
                        val istrazivanjeId = jsonArray.getJSONObject(i).getInt("IstrazivanjeId")

                        grupe.add(Grupa(id,naziv,istrazivanjeId,null))
                    }
                    return grupe
                }
            }
        }catch (e: Exception){
            println("Greska: $e")
        }
        return emptyList()
    }

    fun getGrupeZaIstrazivanje(idIstrazivanja:Int):List<Grupa>{
        val lista = mutableListOf<Grupa>()
        for(i in getGrupe()){
            if(i.istrazivanjeId == idIstrazivanja)
                lista.add(i)
        }
        return lista
    }

    fun upisiUGrupu(idGrupa:Int):Boolean{
        try {
            val url = URL (ApiConfig.baseURL+"/grupa/$idGrupa/student/${AccountRepository.acHash}")
            (url.openConnection() as? HttpURLConnection)?.run {
                this.requestMethod = "POST"

                BufferedReader(InputStreamReader(inputStream)).use {
                    val response = StringBuffer()

                    var inputLine = it.readLine()
                    while (inputLine != null) {
                        response.append(inputLine)
                        inputLine = it.readLine()
                    }
                    //println("Response : $response")

                    return true
                }
            }
        }catch (e: Exception){
            println("Greska: $e")
        }
        return false
    }

    fun getUpisaneGrupe():List<Grupa>{
        val grupe = mutableListOf<Grupa>()
        try {
            val url = URL (ApiConfig.baseURL+"/student/${AccountRepository.acHash}/grupa")
            (url.openConnection() as? HttpURLConnection)?.run {
                BufferedReader(InputStreamReader(inputStream)).use {
                    val response = StringBuffer()

                    var inputLine = it.readLine()
                    while (inputLine != null) {
                        response.append(inputLine)
                        inputLine = it.readLine()
                    }
                    //println("Response : $response")

                    val jsonArray = JSONTokener(response.toString()).nextValue() as JSONArray
                    for (i in 0 until jsonArray.length()) {
                        // ID
                        val id = jsonArray.getJSONObject(i).getInt("id")
                        val naziv = jsonArray.getJSONObject(i).getString("naziv")
                        val istrazivanjeId = jsonArray.getJSONObject(i).getInt("IstrazivanjeId")

                        grupe.add(Grupa(id,naziv,istrazivanjeId,null))
                    }
                    return grupe
                }
            }
        }catch (e: Exception){
            println("Greska: $e")
        }
        return emptyList()
    }
}