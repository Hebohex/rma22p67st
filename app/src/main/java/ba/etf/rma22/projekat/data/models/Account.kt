package ba.etf.rma22.projekat.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class Account(
    @PrimaryKey(autoGenerate = false)
    var id: Int,
    var student: String,
    var acHash: String
    )