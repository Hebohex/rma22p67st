package ba.etf.rma22.projekat.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class Anketa(
    @PrimaryKey(autoGenerate = false)
    var id: Int,
    var naziv: String,
    var datumPocetak: Date,
    var datumKraj: Date,
    var trajanje: Int,
    var anketaiGrupe: Int?,
    var progres: Int,
    var istrazivanje: String,
    var grupa: String
    )