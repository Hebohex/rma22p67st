package ba.etf.rma22.projekat.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Odgovor(
    @PrimaryKey(autoGenerate = false)
    val odgovoreno: Int,
    val anketaTakenId: Int,
    val pitanjeId: Int
)