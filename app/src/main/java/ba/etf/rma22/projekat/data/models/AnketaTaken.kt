package ba.etf.rma22.projekat.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class AnketaTaken(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    var student: String,
    var progres: Int,
    var datumRada: String,
    val AnketumId: Int?
    )