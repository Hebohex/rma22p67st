package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.data.models.AnketaTaken
import org.json.JSONArray
import org.json.JSONObject
import org.json.JSONTokener
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL

object TakeAnketaRepository {
    private val hash = AccountRepository.getHash()
    val pocete = getPoceteAnkete()

    fun zapocniAnketu(idAnkete:Int): AnketaTaken? {
        getPoceteAnkete()?.forEach {
            if(it.AnketumId == idAnkete)
                return null
        }
        try {
            val url = URL (ApiConfig.baseURL+"/student/$hash/anketa/$idAnkete")
            (url.openConnection() as? HttpURLConnection)?.run {
                this.requestMethod = "POST"
                BufferedReader(InputStreamReader(inputStream)).use {
                    val response = StringBuffer()

                    var inputLine = it.readLine()
                    while (inputLine != null) {
                        response.append(inputLine)
                        inputLine = it.readLine()
                    }
                    //println("Response : $response")

                    val jsonObject = JSONTokener(response.toString()).nextValue() as JSONObject
                    val id = jsonObject.getInt("id")
                    val student = jsonObject.getString("student")
                    val progres = jsonObject.getInt("progres")
                    val datumRada = jsonObject.getString("datumRada")
                    val anketumId = jsonObject.getInt("AnketumId")

                    return AnketaTaken(id,student,progres,datumRada,anketumId)
                }
            }
        }catch (e: Exception){
            println("Greska: $e")
        }
        return null
    }

    fun getPoceteAnkete():List<AnketaTaken>?{
        val lista = mutableListOf<AnketaTaken>()
        try {
            val url = URL (ApiConfig.baseURL+"/student/$hash/anketataken")
            (url.openConnection() as? HttpURLConnection)?.run {
                BufferedReader(InputStreamReader(inputStream)).use {
                    val response = StringBuffer()

                    var inputLine = it.readLine()
                    while (inputLine != null) {
                        response.append(inputLine)
                        inputLine = it.readLine()
                    }
                    //println("Response : $response")

                    val jsonArray = JSONTokener(response.toString()).nextValue() as JSONArray
                    for (i in 0 until jsonArray.length()) {
                        // ID
                        val id = jsonArray.getJSONObject(i).getInt("id")
                        val student = jsonArray.getJSONObject(i).getString("student")
                        val progres = jsonArray.getJSONObject(i).getInt("progres")
                        val datumRada = jsonArray.getJSONObject(i).getString("datumRada")
                        val anketumId = jsonArray.getJSONObject(i).getInt("AnketumId")

                        lista.add(AnketaTaken(id,student,progres,datumRada,anketumId))
                    }
                    return lista
                }
            }
        }catch (e: Exception){
            println("Greska: $e")
        }
        return null
    }
}