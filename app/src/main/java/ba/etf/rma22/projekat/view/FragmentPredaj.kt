package ba.etf.rma22.projekat.view

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import ba.etf.rma22.projekat.Communicator
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.repositories.OdgovorRepository
import ba.etf.rma22.projekat.data.repositories.PitanjeAnketaRepository
import ba.etf.rma22.projekat.viewmodel.AnketaViewModel
import java.util.*

class FragmentPredaj(
    private var anketa: Anketa,
    private var status: String
) : Fragment() {

    private lateinit var tekstProgres : TextView
    private lateinit var predaj : Button
    private lateinit var communicator: Communicator
    private lateinit var anketaViewModel:AnketaViewModel

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_predaj, container, false)

        anketaViewModel = ViewModelProvider(this).get(AnketaViewModel::class.java)

        tekstProgres = view.findViewById(R.id.progresTekst)
        predaj = view.findViewById(R.id.dugmePredaj)

        communicator = activity as Communicator

        tekstProgres.text = postotak(anketa.progres)
        if(status!="zelena" || !checkForInternet())
            predaj.isEnabled = false

        if(anketa.progres !=100)

        predaj.setOnClickListener {
            anketaViewModel.getPoceteAnkete()
            anketaViewModel.pocete.observe(viewLifecycleOwner, androidx.lifecycle.Observer { poceta->
                var id = -1
                poceta.forEach {
                    if(anketa.id == it.AnketumId) {
                        id = it.id
                    }
                }
                if(id!=-1){
                    PitanjeAnketaRepository.pitanja.forEach {
                        anketaViewModel.postaviOdgovorAnketa(id,it.id, it.odgovoreno)
                    }
                    anketaViewModel.azurirajPitanja(PitanjeAnketaRepository.pitanja)
                    var poruka="Spremljeni su odgovori na anketu ${anketa.naziv}\n u okviru istraživanja " + anketa.istrazivanje
                    if(anketa.progres==100)
                        poruka = "Završili ste anketu ${anketa.naziv} u okviru istraživanja\n" + anketa.istrazivanje

                    communicator = activity as Communicator
                    communicator.passDataCon(poruka)
                }
            })
        }

        return view
    }

    @SuppressLint("SetTextI18n")
    override fun onResume() {
        super.onResume()
        val x = (anketa.progres)
        tekstProgres.text = postotak(x)
    }

    private fun postotak(x: Int):String{
        return when{
            x == 0 -> "0%"
            x <= 30 -> "20%"
            x <= 50 -> "40%"
            x <= 70 -> "60%"
            x <= 90 -> "80%"
            else -> "100%"
        }
    }

    companion object {
        fun newInstance(anketa: Anketa,status: String) = FragmentPredaj(anketa,status)
    }

    fun checkForInternet(): Boolean {
        val context = requireContext()
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val network = connectivityManager.activeNetwork ?: return false
            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false
            return when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        } else {
            @Suppress("DEPRECATION") val networkInfo =
                connectivityManager.activeNetworkInfo ?: return false
            @Suppress("DEPRECATION")
            return networkInfo.isConnected
        }
    }
}