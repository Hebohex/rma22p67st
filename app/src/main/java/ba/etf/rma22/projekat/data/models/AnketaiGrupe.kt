package ba.etf.rma22.projekat.data.models

data class AnketaiGrupe(
    val grupaId: Int,
    val anketumId: Int
)