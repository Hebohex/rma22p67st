package ba.etf.rma22.projekat.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Grupa(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val naziv: String,
    val istrazivanjeId: Int,
    val anketaIGrupe: Int?
)