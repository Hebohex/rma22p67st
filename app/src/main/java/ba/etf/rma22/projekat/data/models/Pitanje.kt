package ba.etf.rma22.projekat.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Pitanje(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    var naziv: String,
    var tekstPitanja: String,
    var opcije: List<String>,
    var pitanjeAnketa: Int?,
    var odgovoreno: Int
)