package ba.etf.rma22.projekat.data.database

import android.content.Context
import androidx.room.*
import ba.etf.rma22.projekat.Converter
import ba.etf.rma22.projekat.data.models.*

@Database(
    entities = [
        Account::class,
        Anketa::class,
        AnketaTaken::class,
        Grupa::class,
        Istrazivanje::class,
        Odgovor::class,
        Pitanje::class
    ],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converter::class)
abstract class AnketaDatabase : RoomDatabase() {

    abstract fun anketaDao(): AnketaDao

    companion object {
        @Volatile
        private var INSTANCE: AnketaDatabase? = null

        fun getDatabase(context: Context): AnketaDatabase{
            val tempInstance = INSTANCE
            if(tempInstance != null){
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AnketaDatabase::class.java,
                    "RMA22DB"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}