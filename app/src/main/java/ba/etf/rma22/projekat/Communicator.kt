package ba.etf.rma22.projekat

import ba.etf.rma22.projekat.data.models.Anketa

interface Communicator {
    fun passDataCon(poruka: String)

    fun vratiFragment()

    fun pitanjaAnkete(anketa: Anketa,status: String)

    fun vratiNaPocetnu()
}