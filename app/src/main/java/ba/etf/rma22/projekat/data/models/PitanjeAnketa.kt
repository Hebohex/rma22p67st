package ba.etf.rma22.projekat.data.models

data class PitanjeAnketa(
    val anketumId: Int,
    val pitanjeId: Int
)