package ba.etf.rma22.projekat.viewmodel

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ba.etf.rma22.projekat.data.database.AnketaDao
import ba.etf.rma22.projekat.data.database.AnketaDatabase
import ba.etf.rma22.projekat.data.models.Account
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.repositories.AnketaRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class AnketaListViewModel(application: Application): AndroidViewModel(application) {

    var response:MutableLiveData<List<Anketa>> = MutableLiveData()
    private val account: LiveData<List<Account>>
    val anketaDao: AnketaDao
    var anketa: LiveData<List<Anketa>>

    init {
        anketaDao = AnketaDatabase.getDatabase(
            application
        ).anketaDao()
        account = anketaDao.account()
        anketa = anketaDao.anketa()
    }

    fun addAccount(account: Account){
        GlobalScope.launch(Dispatchers.IO) {
            anketaDao.addAccount(account)
        }
    }

    fun addAnketa(anketa:Anketa){
        GlobalScope.launch(Dispatchers.IO) {
            anketaDao.addAnketa(anketa)
        }
    }

    fun anketeBaze(){
        GlobalScope.launch(Dispatchers.IO) {
            anketa = anketaDao.anketa()
        }
    }

    fun getMyAnkete() {
        GlobalScope.launch(Dispatchers.IO) {
            val res = AnketaRepository.getMyAnkete().sortedBy { anketa -> anketa.datumPocetak }
            response.postValue(res)
            res.forEach {
                anketaDao.addAnketa(it)
            }
        }
    }
    fun getAll() {
        GlobalScope.launch(Dispatchers.IO) {
            response.postValue(AnketaRepository.getAll().sortedBy { anketa -> anketa.datumPocetak })
        }
    }
    fun getDone(){
        GlobalScope.launch(Dispatchers.IO) {
            response.postValue(AnketaRepository.getDone().sortedBy { anketa -> anketa.datumPocetak })
        }
    }
    fun getFuture(){
        GlobalScope.launch(Dispatchers.IO) {
            response.postValue(AnketaRepository.getFuture().sortedBy { anketa -> anketa.datumPocetak })
        }
    }
    fun getNotTaken() {
        GlobalScope.launch(Dispatchers.IO) {
            response.postValue(AnketaRepository.getNotTaken().sortedBy { anketa -> anketa.datumPocetak })
        }
    }
}