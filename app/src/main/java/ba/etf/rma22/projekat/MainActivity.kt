package ba.etf.rma22.projekat

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.data.repositories.AccountRepository
import ba.etf.rma22.projekat.data.repositories.PitanjeAnketaRepository
import ba.etf.rma22.projekat.view.*
import ba.etf.rma22.projekat.viewmodel.AnketaListViewModel
import ba.etf.rma22.projekat.viewmodel.AnketaViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class MainActivity : AppCompatActivity(), Communicator {

    private lateinit var viewPager: ViewPager2
    private lateinit var viewPagerAdapter: ViewPagerAdapter
    private lateinit var anketaViewModel:AnketaViewModel
    private lateinit var anketaListViewModel: AnketaListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        anketaListViewModel = ViewModelProvider(this).get(AnketaListViewModel::class.java)
        anketaViewModel = ViewModelProvider(this).get(AnketaViewModel::class.java)

        val intent = intent
        if (intent != null) {
            val acc = intent.getStringExtra("payload")
            if(acc!=null){
                Log.e("AccHash:", "$acc")
                if(AccountRepository.postaviHash(acc)){
                    GlobalScope.launch(Dispatchers.IO) {
                        deleteDatabase("RMA22DB")
                        anketaListViewModel.addAccount(AccountRepository.getStudent()!!)
                    }
                }
            }
        }

        viewPager = findViewById(R.id.pager)
        val fragments =
            mutableListOf(
                FragmentAnkete(),
                FragmentIstrazivanje(),
            )

        viewPager.offscreenPageLimit = 1
        viewPagerAdapter = ViewPagerAdapter(supportFragmentManager, fragments, lifecycle)
        viewPager.adapter = viewPagerAdapter

        /*
        val myPageChangeCallback = object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                if(position == 0)
                vratiFragment()
            }
        }
        viewPager.registerOnPageChangeCallback(myPageChangeCallback)
        */
    }

    override fun passDataCon(poruka: String) {
        val bundle = Bundle()
        bundle.putString("poruka", poruka)
        vratiNaPocetnu()

        Handler(Looper.getMainLooper()).postDelayed({
            viewPagerAdapter.replaceFragment(1, FragmentPoruka(), bundle)
            viewPager.currentItem = 1
            for(i in viewPagerAdapter.itemCount-1 downTo 2)
                viewPagerAdapter.remove(i)
        }, 100)
    }

    override fun vratiFragment() {
        Handler(Looper.getMainLooper()).postDelayed({
            viewPagerAdapter.refreshFragment(1, FragmentIstrazivanje())
        }, 0)
    }

    //2. zadatak
    override fun pitanjaAnkete(anketa: Anketa,status: String) {

        if(checkForInternet()) {
            anketaViewModel.odgovorene(anketa.id)
            anketaViewModel.getPitanja(anketa.id)
            anketaViewModel.pitanja.observe(this, Observer {
                PitanjeAnketaRepository.pitanja = it as MutableList<Pitanje>
                if(!it.isNullOrEmpty())
                Handler(Looper.getMainLooper()).postDelayed({
                    viewPagerAdapter.refreshFragment(
                        0,
                        FragmentPitanje.newInstance(anketa, status, 0)
                    )
                    for (i in 1 until viewPagerAdapter.itemCount)
                        viewPagerAdapter.remove(1)

                    for (i in 1 until it.size) {
                        viewPagerAdapter.add(i, FragmentPitanje.newInstance(anketa, status, i))
                    }

                    viewPagerAdapter.add(it.size, FragmentPredaj.newInstance(anketa, status))
                }, 0)
            })
        }else{
            anketaViewModel.pitanjaBaze()
            anketaViewModel.pitanjaBaza.observe(this, Observer {
                Handler(Looper.getMainLooper()).postDelayed({
                    viewPagerAdapter.refreshFragment(
                        0,
                        FragmentPitanje.newInstance(anketa, status, 0)
                    )
                    for (i in 1 until viewPagerAdapter.itemCount)
                        viewPagerAdapter.remove(1)

                    for (i in 1 until it.size) {
                        viewPagerAdapter.add(i, FragmentPitanje.newInstance(anketa, status, i))
                    }

                    //viewPagerAdapter.add(it.size, FragmentPredaj.newInstance(anketa, status))
                }, 0)
            })
        }

    }

    override fun vratiNaPocetnu() {
        Handler(Looper.getMainLooper()).postDelayed({
            viewPager.currentItem = 0
            viewPager = findViewById(R.id.pager)
            val fragments =
                mutableListOf(
                    FragmentAnkete(),
                    FragmentIstrazivanje(),
                )
            viewPagerAdapter = ViewPagerAdapter(supportFragmentManager, fragments, lifecycle)
            viewPager.adapter = viewPagerAdapter
        }, 0)
    }

    fun checkForInternet(): Boolean {
        val context = this
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val network = connectivityManager.activeNetwork ?: return false
            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false
            return when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        } else {
            @Suppress("DEPRECATION") val networkInfo =
                connectivityManager.activeNetworkInfo ?: return false
            @Suppress("DEPRECATION")
            return networkInfo.isConnected
        }
    }

}