package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.data.models.PitanjeAnketa
import org.json.JSONArray
import org.json.JSONTokener
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

object PitanjeAnketaRepository {
    var pitanja = mutableListOf<Pitanje>()
    var odgovoreni = listOf<Pitanje>()

    fun getPitanja(idAnkete:Int):List<Pitanje>{
        val lista = mutableListOf<Pitanje>()
        var idAnk = -1
        TakeAnketaRepository.getPoceteAnkete()!!.forEach {
            if(it.AnketumId == idAnkete)
                idAnk= it.id
        }
        val odgovori = OdgovorRepository.getOdgovoriAnketa(idAnk)
        try {
            val url = URL (ApiConfig.baseURL+"/anketa/$idAnkete/pitanja")
            (url.openConnection() as? HttpURLConnection)?.run {
                BufferedReader(InputStreamReader(inputStream)).use {
                    val response = StringBuffer()

                    var inputLine = it.readLine()
                    while (inputLine != null) {
                        response.append(inputLine)
                        inputLine = it.readLine()
                    }
                    //println("Response : $response")

                    val jsonArray = JSONTokener(response.toString()).nextValue() as JSONArray
                    for (i in 0 until jsonArray.length()) {
                        // ID
                        val id = jsonArray.getJSONObject(i).getInt("id")
                        val naziv = jsonArray.getJSONObject(i).getString("naziv")
                        val tekstPitanja = jsonArray.getJSONObject(i).getString("tekstPitanja")
                        val opcije = mutableListOf<String>()
                        val opc = jsonArray.getJSONObject(i).getJSONArray("opcije")
                        for(i in 0 until opc.length()){
                            opcije.add(opc.getString(i))
                        }
                        val anketumId = jsonArray.getJSONObject(i).getJSONObject("PitanjeAnketa").getInt("AnketumId")
                        val pitanjeId = jsonArray.getJSONObject(i).getJSONObject("PitanjeAnketa").getInt("PitanjeId")
                        var odgovor = -1
                        odgovori.forEach { odg->
                            if(odg.pitanjeId == id && odg.anketaTakenId==idAnk)
                                odgovor = odg.odgovoreno
                        }

                        lista.add(Pitanje(id,naziv,tekstPitanja,opcije, anketumId,odgovor))
                    }
                    return lista
                }
            }
        }catch (e: Exception){
            println("Greska: $e")
        }
        return emptyList()
    }
}