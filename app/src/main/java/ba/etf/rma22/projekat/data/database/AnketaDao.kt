package ba.etf.rma22.projekat.data.database

import androidx.lifecycle.LiveData
import androidx.room.*
import ba.etf.rma22.projekat.data.models.*

@Dao
interface AnketaDao {

    //Account
    @Query("SELECT * FROM Account")
    fun account(): LiveData<List<Account>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addAccount(account: Account)

    //Ankete
    @Query("SELECT * FROM Anketa")
    fun anketa(): LiveData<List<Anketa>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addAnketa(anketa: Anketa)

    @Update
    suspend fun updateAnketa(anketa: Anketa)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addAnketaTaken(anketaTaken: AnketaTaken)

    //Pitanja
    @Query("SELECT * FROM Pitanje")
    fun pitanja(): LiveData<List<Pitanje>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun dodajPitanje(pitanje: Pitanje)

    @Update
    suspend fun updatePitanje(pitanje: Pitanje)

    @Query("DELETE FROM Pitanje")
    suspend fun deleteAllPitanja()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun dodajIstrazivanje(istrazivanje: Istrazivanje)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun dodajGrupu(grupa: Grupa)
}