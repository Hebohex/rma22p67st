package ba.etf.rma22.projekat.viewmodel

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import androidx.core.content.ContextCompat.getSystemService
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ba.etf.rma22.projekat.data.database.AnketaDao
import ba.etf.rma22.projekat.data.database.AnketaDatabase
import ba.etf.rma22.projekat.data.models.*
import ba.etf.rma22.projekat.data.repositories.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class AnketaViewModel(application: Application): AndroidViewModel(application) {

    var pitanja: MutableLiveData<List<Pitanje>> = MutableLiveData()
    var progres: MutableLiveData<Int> = MutableLiveData()
    var pocete: MutableLiveData<List<AnketaTaken>> = MutableLiveData()
    var anketa: MutableLiveData<AnketaTaken> = MutableLiveData()
    val anketaDao: AnketaDao
    var pitanjaBaza: LiveData<List<Pitanje>>

    init {
        anketaDao = AnketaDatabase.getDatabase(
            application
        ).anketaDao()
        pitanjaBaza = anketaDao.pitanja()
    }

    fun pitanjaBaze(){
        GlobalScope.launch(Dispatchers.IO) {
            pitanjaBaza = anketaDao.pitanja()
        }
    }

    fun azurirajPitanja(lista: List<Pitanje>){
        GlobalScope.launch(Dispatchers.IO) {
            //anketaDao.deleteAllPitanja()
            lista.forEach {
                anketaDao.dodajPitanje(it)
            }
        }
    }


    fun getPitanja(anketaId: Int) {
        GlobalScope.launch(Dispatchers.IO) {
            pitanja.postValue(PitanjeAnketaRepository.getPitanja(anketaId))
        }
    }

    fun odgovorene(anketaId: Int){
        GlobalScope.launch(Dispatchers.IO) {
            PitanjeAnketaRepository.odgovoreni = PitanjeAnketaRepository.getPitanja(anketaId)
        }
    }

    fun postaviOdgovorAnketa(idAnketaTaken:Int, idPitanje:Int, odgovor:Int){
        GlobalScope.launch(Dispatchers.IO) {
            progres.postValue(OdgovorRepository.postaviOdgovorAnketa(idAnketaTaken, idPitanje, odgovor))
        }
    }

    fun getPoceteAnkete(){
        GlobalScope.launch(Dispatchers.IO) {
            pocete.postValue(TakeAnketaRepository.getPoceteAnkete())
        }
    }

    fun zapocniAnketu(idAnkete:Int){
        GlobalScope.launch(Dispatchers.IO) {
            val res = TakeAnketaRepository.zapocniAnketu(idAnkete)
            anketa.postValue(res)
            if (res != null) {
                anketaDao.addAnketaTaken(res)
            }
        }
    }
}