package ba.etf.rma22.projekat.view

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma22.projekat.Communicator
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.viewmodel.AnketaListViewModel

class FragmentAnkete : Fragment(),AnketaListAdapter.OnItemClickListener {

    private lateinit var ankete: RecyclerView
    private lateinit var anketeAdapter: AnketaListAdapter
    private lateinit var anketaListViewModel:AnketaListViewModel
    private lateinit var filter: Spinner
    private lateinit var communicator: Communicator

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.fragment_ankete, container, false)
        ankete = view.findViewById(R.id.listaAnketa)

        /* ankete prikazane horizontalno
        ankete.layoutManager = LinearLayoutManager(
            null,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        */
        ankete.layoutManager = GridLayoutManager(activity, 1)
        anketeAdapter = AnketaListAdapter(arrayListOf(),this)
        ankete.adapter=anketeAdapter

        anketaListViewModel = ViewModelProvider(this).get(AnketaListViewModel::class.java)

        if(checkForInternet()) {
            anketaListViewModel.response.observe(viewLifecycleOwner, Observer {
                anketeAdapter.updateAnkete(it)
            })
        }else{
            anketaListViewModel.anketa.observe(viewLifecycleOwner, Observer { it2 ->
                anketeAdapter.updateAnkete(it2)
            })
        }
        val listaOdabira = listOf(
            "Sve moje ankete",
            "Sve ankete",
            "Urađene ankete",
            "Buduće ankete",
            "Prošle ankete"
        )
        // access the spinner
        filter = view.findViewById(R.id.filterAnketa)
        val adapter = activity?.applicationContext?.let {
            ArrayAdapter(
                it,
                android.R.layout.simple_spinner_item, listaOdabira)
        }
        adapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        filter.adapter = adapter

        if(!checkForInternet())
            filter.isEnabled = false

        filter.onItemSelectedListener= object : AdapterView.OnItemSelectedListener{

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                anketeAdapter.updateAnkete(listOf())
                when (position) {
                    0 -> {
                        if(checkForInternet()){
                            anketaListViewModel.getMyAnkete()
                            anketaListViewModel.response.observe(viewLifecycleOwner, Observer {
                                anketeAdapter.updateAnkete(it)
                            })
                        }else{
                            anketaListViewModel.anketeBaze()
                            anketaListViewModel.anketa.observe(viewLifecycleOwner, Observer { it2->
                                anketeAdapter.updateAnkete(it2)
                            })
                        }
                    }
                    1 -> {
                        anketaListViewModel.getAll()
                        anketaListViewModel.response.observe(viewLifecycleOwner, Observer {
                            anketeAdapter.updateAnkete(it.distinctBy { a -> a.naziv })
                        })
                    }
                    2 -> {
                        anketaListViewModel.getDone()
                        anketaListViewModel.response.observe(viewLifecycleOwner, Observer {
                            anketeAdapter.updateAnkete(it)
                        })
                    }
                    3 -> {
                        anketaListViewModel.getFuture()
                        anketaListViewModel.response.observe(viewLifecycleOwner, Observer {
                            anketeAdapter.updateAnkete(it)
                        })
                    }
                    4 -> {
                        anketaListViewModel.getNotTaken()
                        anketaListViewModel.response.observe(viewLifecycleOwner, Observer {
                            anketeAdapter.updateAnkete(it)
                        })
                    }
                }
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

        }
        return view
    }

    override fun onItemClick(anketa: String, istrazivanje: String, status: String) {
        communicator = activity as Communicator
        if(checkForInternet()) {
            anketaListViewModel.response.observe(viewLifecycleOwner, Observer {
                for (i in it) {
                    if (i.naziv == anketa && i.istrazivanje == istrazivanje && status != "zuta") {
                        communicator.pitanjaAnkete(i, status)
                        break
                    }
                }
            })
        }else{
            anketaListViewModel.anketa.observe(viewLifecycleOwner, Observer { it2 ->
                for (i in it2) {
                    if (i.naziv == anketa && i.istrazivanje == istrazivanje && status != "zuta") {
                        communicator.pitanjaAnkete(i, status)
                        break
                    }
                }
            })
        }
    }

    fun checkForInternet(): Boolean {
        val context = requireContext()
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val network = connectivityManager.activeNetwork ?: return false
            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false
            return when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        } else {
            @Suppress("DEPRECATION") val networkInfo =
                connectivityManager.activeNetworkInfo ?: return false
            @Suppress("DEPRECATION")
            return networkInfo.isConnected
        }
    }
}