package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.data.models.Anketa
import org.json.JSONArray
import org.json.JSONObject
import org.json.JSONTokener
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*

object AnketaRepository {
    var ankete = getAll()

    fun getMyAnkete() : List<Anketa>{
        val list = mutableListOf<Anketa>()
        IstrazivanjeIGrupaRepository.getUpisaneGrupe().forEach { gr ->
            try {
                val url = URL (ApiConfig.baseURL+"/grupa/${gr.id}/ankete")
                (url.openConnection() as? HttpURLConnection)?.run {
                    BufferedReader(InputStreamReader(inputStream)).use {
                        val response = StringBuffer()

                        var inputLine = it.readLine()
                        while (inputLine != null) {
                            response.append(inputLine)
                            inputLine = it.readLine()
                        }
                        //println("Response : $response")

                        val jsonArray = JSONTokener(response.toString()).nextValue() as JSONArray
                        for (i in 0 until jsonArray.length()) {
                            // ID
                            val id = jsonArray.getJSONObject(i).getInt("id")
                            val naziv = jsonArray.getJSONObject(i).getString("naziv")
                            val trajanje = jsonArray.getJSONObject(i).getInt("trajanje")

                            val cal = Calendar.getInstance()
                            val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

                            val pocetak = jsonArray.getJSONObject(i).getString("datumPocetak")
                            cal.time = sdf.parse(pocetak)!!
                            val datumPocetak = cal.time

                            val kraj = jsonArray.getJSONObject(i).getString("datumKraj")

                            cal.add(Calendar.DAY_OF_MONTH,trajanje)
                            val datumKraj = cal.time

                            val grupaId = jsonArray.getJSONObject(i).getJSONObject("AnketaiGrupe").getInt("GrupaId")
                            val anketumId = jsonArray.getJSONObject(i).getJSONObject("AnketaiGrupe").getInt("AnketumId")

                            val istrazivanje = IstrazivanjeIGrupaRepository.getIstrazivanjeById(gr.istrazivanjeId)!!.naziv

                            val pitanja = PitanjeAnketaRepository.getPitanja(id)
                            var odgovoreno:Double = 0.0
                            pitanja.forEach { pit->
                                if(pit.odgovoreno != -1)
                                    odgovoreno++
                            }

                            val progres = ((odgovoreno/pitanja.size)*100).toInt()
                            var da = true
                            list.forEach { an->
                                if(an.id == id )
                                    da = false
                            }
                            if(da)
                                list.add(Anketa(id,naziv, datumPocetak, datumKraj, trajanje,
                                    grupaId,progres,istrazivanje,gr.naziv))
                        }
                    }
                }
            }catch (e: Exception){
                println("Greska: $e")
            }
        }
        return list
    }

    fun getAll() : List<Anketa> {
        var lista = mutableListOf<Anketa>()
        val poz = listOf(1,2)
        poz.forEach { a->
            try {
                val url = URL(ApiConfig.baseURL + "/anketa?offset=$a")
                (url.openConnection() as? HttpURLConnection)?.run {
                    BufferedReader(InputStreamReader(inputStream)).use {
                        val response = StringBuffer()

                        var inputLine = it.readLine()
                        while (inputLine != null) {
                            response.append(inputLine)
                            inputLine = it.readLine()
                        }
                        //println("Response : $response")

                        val jsonArray = JSONTokener(response.toString()).nextValue() as JSONArray
                        for (i in 0 until jsonArray.length()) {
                            // ID
                            val id = jsonArray.getJSONObject(i).getInt("id")
                            val naziv = jsonArray.getJSONObject(i).getString("naziv")
                            val trajanje = jsonArray.getJSONObject(i).getInt("trajanje")

                            val cal = Calendar.getInstance()
                            val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

                            val pocetak = jsonArray.getJSONObject(i).getString("datumPocetak")
                            cal.time = sdf.parse(pocetak)!!
                            val datumPocetak = cal.time

                            val kraj = jsonArray.getJSONObject(i).getString("datumKraj")

                            cal.add(Calendar.DAY_OF_MONTH, trajanje)
                            val datumKraj = cal.time

                            lista.add(
                                Anketa(
                                    id,
                                    naziv,
                                    datumPocetak,
                                    datumKraj,
                                    trajanje,
                                    null,
                                    0,
                                    "",
                                    ""
                                )
                            )
                        }
                    }
                }
            } catch (e: Exception) {
                println("Greska: $e")
            }
        }
        return lista
    }

    fun getAnketeZaGrupu(idGrupe: Int) : List<Anketa> {
        var lista = mutableListOf<Anketa>()
        try {
            val url = URL(ApiConfig.baseURL + "/grupa/$idGrupe/ankete")
            (url.openConnection() as? HttpURLConnection)?.run {
                BufferedReader(InputStreamReader(inputStream)).use {
                    val response = StringBuffer()

                    var inputLine = it.readLine()
                    while (inputLine != null) {
                        response.append(inputLine)
                        inputLine = it.readLine()
                    }
                    //println("Response : $response")

                    val jsonArray = JSONTokener(response.toString()).nextValue() as JSONArray
                    for (i in 0 until jsonArray.length()) {
                        // ID
                        val id = jsonArray.getJSONObject(i).getInt("id")
                        val naziv = jsonArray.getJSONObject(i).getString("naziv")
                        val trajanje = jsonArray.getJSONObject(i).getInt("trajanje")

                        val cal = Calendar.getInstance()
                        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

                        val pocetak = jsonArray.getJSONObject(i).getString("datumPocetak")
                        cal.time = sdf.parse(pocetak)!!
                        val datumPocetak = cal.time

                        val kraj = jsonArray.getJSONObject(i).getString("datumKraj")

                        cal.add(Calendar.DAY_OF_MONTH, trajanje)
                        val datumKraj = cal.time

                        val grupaId = jsonArray.getJSONObject(i).getJSONObject("AnketaiGrupe").getInt("GrupaId")
                        val anketumId = jsonArray.getJSONObject(i).getJSONObject("AnketaiGrupe").getInt("AnketumId")

                        lista.add(
                            Anketa(
                                id,
                                naziv,
                                datumPocetak,
                                datumKraj,
                                trajanje,
                                grupaId,
                                0,
                                "",
                                ""
                            )
                        )
                    }
                }
            }
        } catch (e: Exception) {
            println("Greska: $e")
        }
        return lista
    }

    fun getById(id:Int):Anketa?{
        try {
            val url = URL (ApiConfig.baseURL+"/anketa/$id")
            (url.openConnection() as? HttpURLConnection)?.run {
                BufferedReader(InputStreamReader(inputStream)).use {
                    val response = StringBuffer()

                    var inputLine = it.readLine()
                    while (inputLine != null) {
                        response.append(inputLine)
                        inputLine = it.readLine()
                    }
                    //println("Response : $response")

                    val jsonObject = JSONTokener(response.toString()).nextValue() as JSONObject
                    val id = jsonObject.getInt("id")
                    val naziv = jsonObject.getString("naziv")
                    val trajanje = jsonObject.getInt("trajanje")

                    val cal = Calendar.getInstance()
                    val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

                    val pocetak = jsonObject.getString("datumPocetak")
                    cal.time = sdf.parse(pocetak)!!
                    val datumPocetak = cal.time

                    val kraj = jsonObject.getString("datumKraj")

                    cal.add(Calendar.DAY_OF_MONTH, trajanje)
                    val datumKraj = cal.time

                    return Anketa(id,naziv,datumPocetak,datumKraj,trajanje,null,0,"","")
                }
            }
        }catch (e: Exception){
            println("Greska: $e")
        }
        return null
    }

    fun getUpisane():List<Anketa>{
        return getMyAnkete()
    }

    fun getDone() : List<Anketa> {
        val lista : MutableList<Anketa> = arrayListOf()
        for(i in getMyAnkete()) {
            if(i.progres == 100)
                lista.add(i)
        }
        return lista
    }

    fun getFuture() : List<Anketa> {
        val lista : MutableList<Anketa> = arrayListOf()
        val cal: Calendar = Calendar.getInstance()
        val date: Date = cal.time
        for(i in getMyAnkete()) {
            if (i.datumPocetak>date) {
                lista.add(i)
            }
        }
        return lista
    }

    fun getNotTaken() : List<Anketa> {
        val lista : MutableList<Anketa> = arrayListOf()
        val cal: Calendar = Calendar.getInstance()
        val date: Date = cal.time
        for(i in getMyAnkete()) {
            for(j in TakeAnketaRepository.pocete!!){
                if(i.id==j.AnketumId && j.progres != 100 && i.datumKraj<date)
                    lista.add(i)
            }
        }
        return lista
    }

}