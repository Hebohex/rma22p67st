package ba.etf.rma22.projekat.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ba.etf.rma22.projekat.Communicator
import ba.etf.rma22.projekat.R

class FragmentPoruka : Fragment() {

    private lateinit var communicator: Communicator

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_poruka, container, false)

        view.findViewById<TextView>(R.id.tvPoruka).text = arguments?.getString("poruka")

        return view
    }

    override fun onPause() {
        super.onPause()
        communicator = activity as Communicator
        communicator.vratiFragment()
    }

}