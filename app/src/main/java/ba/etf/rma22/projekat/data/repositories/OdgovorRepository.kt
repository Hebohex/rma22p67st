package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.data.models.Odgovor
import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.data.models.PitanjeAnketa
import org.json.JSONArray
import org.json.JSONObject
import org.json.JSONTokener
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL

object OdgovorRepository {
    private val hash = AccountRepository.acHash
    var progres = 0

    fun getOdgovoriAnketa(idAnkete:Int?):List<Odgovor>{
        val lista = mutableListOf<Odgovor>()
        try {
            val url = URL (ApiConfig.baseURL+"/student/$hash/anketataken/$idAnkete/odgovori")
            (url.openConnection() as? HttpURLConnection)?.run {
                BufferedReader(InputStreamReader(inputStream)).use {
                    val response = StringBuffer()

                    var inputLine = it.readLine()
                    while (inputLine != null) {
                        response.append(inputLine)
                        inputLine = it.readLine()
                    }
                    //println("Response : $response")
                    //println(response.toString().substring(252,258))
                    val jsonArray = JSONTokener(response.toString()).nextValue() as JSONArray
                    for (i in 0 until jsonArray.length()) {

                        val odgovoreno = jsonArray.getJSONObject(i).getInt("odgovoreno")
                        val anketaTakenId = jsonArray.getJSONObject(i).getInt("AnketaTakenId")
                        val pitanjeId = jsonArray.getJSONObject(i).getInt("PitanjeId")

                        lista.add(Odgovor(odgovoreno, anketaTakenId, pitanjeId))
                    }
                    return lista
                }
            }
        }catch (e: Exception){
            println("Greska: $e")
        }
        return emptyList()
    }

    fun postaviOdgovorAnketa(idAnketaTaken:Int, idPitanje:Int, odgovor:Int):Int{

        getOdgovoriAnketa(idAnketaTaken).forEach {
            if(it.pitanjeId==idPitanje && it.odgovoreno != -1)
                return progres
        }
        if(odgovor == -1)
            return progres

        //println("Podaci $idAnketaTaken, \"odgovor\": $odgovor,\"pitanje\": $idPitanje,\"progres\": $progres")
        try {
            val url = URL (ApiConfig.baseURL+"/student/${AccountRepository.acHash}/anketataken/$idAnketaTaken/odgovor")
            (url.openConnection() as? HttpURLConnection)?.run {
                this.requestMethod = "POST"
                this.setRequestProperty("Content-Type", "application/json")
                this.doOutput = true
                val wr = OutputStreamWriter(outputStream);
                val reqParam = "{\"odgovor\": $odgovor,\"pitanje\": $idPitanje,\"progres\": $progres}"
                wr.write(reqParam);
                wr.flush();
                BufferedReader(InputStreamReader(inputStream)).use {
                    val response = StringBuffer()

                    var inputLine = it.readLine()
                    while (inputLine != null) {
                        response.append(inputLine)
                        inputLine = it.readLine()
                    }
                    //println("Response : $response")

                    return progres
                }
            }
        }catch (e: Exception){
            println("Greska: $e")
        }
        return -1
    }
}