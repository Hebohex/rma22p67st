package ba.etf.rma22.projekat.view

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import ba.etf.rma22.projekat.Communicator
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.models.Istrazivanje
import ba.etf.rma22.projekat.data.repositories.AnketaRepository
import ba.etf.rma22.projekat.viewmodel.AnketaListViewModel
import ba.etf.rma22.projekat.viewmodel.AnketaViewModel
import ba.etf.rma22.projekat.viewmodel.IstrazivanjeIGrupaViewModel


class FragmentIstrazivanje : Fragment() {

    private lateinit var godina: Spinner
    private lateinit var istrazivanje: Spinner
    private lateinit var grupa: Spinner
    private lateinit var communicator: Communicator
    private lateinit var istrList: List<Istrazivanje>
    private lateinit var grList: List<Grupa>
    private lateinit var igViewModel:IstrazivanjeIGrupaViewModel
    private lateinit var anViewModel : AnketaViewModel
    private lateinit var anListViewModel: AnketaListViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.fragment_istrazivanje, container, false)

        igViewModel = ViewModelProvider(this).get(IstrazivanjeIGrupaViewModel::class.java)
        anViewModel = ViewModelProvider(this).get(AnketaViewModel::class.java)
        anListViewModel = ViewModelProvider(this).get(AnketaListViewModel::class.java)

        view.findViewById<Button>(R.id.dodajIstrazivanjeDugme).isEnabled = false
        val godine = listOf("1", "2", "3", "4", "5")
        // access the spinner
        godina = view.findViewById(R.id.odabirGodina)
        val adapter =activity?.applicationContext?.let {
            ArrayAdapter(
                it,
                android.R.layout.simple_spinner_item, godine)
        }
        adapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        godina.adapter = adapter

        godina.onItemSelectedListener= object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                popuniIstrazivanja(p2+1)
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                view.findViewById<Button>(R.id.dodajIstrazivanjeDugme).isEnabled = false
            }

        }
        view.findViewById<Spinner>(R.id.odabirIstrazivanja).onItemSelectedListener= object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val text: String = p0?.getItemAtPosition(p2).toString()
                if(text=="")
                    view.findViewById<Button>(R.id.dodajIstrazivanjeDugme).isEnabled = false
                else
                    popuniGrupe(text)
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                view.findViewById<Button>(R.id.dodajIstrazivanjeDugme).isEnabled = false
            }

        }
        view.findViewById<Spinner>(R.id.odabirGrupa).onItemSelectedListener= object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val text: String = p0?.getItemAtPosition(p2).toString()
                view.findViewById<Button>(R.id.dodajIstrazivanjeDugme).isEnabled = text != ""
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                view.findViewById<Button>(R.id.dodajIstrazivanjeDugme).isEnabled = false
            }

        }
        view.findViewById<Button>(R.id.dodajIstrazivanjeDugme).setOnClickListener { upisNaAnketu(view)}


        if(!checkForInternet()){
            view.findViewById<Spinner>(R.id.odabirGrupa).isEnabled = false
            view.findViewById<Spinner>(R.id.odabirIstrazivanja).isEnabled = false
            godina.isEnabled = false
        }

        return view
    }

    private fun popuniIstrazivanja(godina: Int) {
        istrazivanje = view?.findViewById(R.id.odabirIstrazivanja)!!
        val listaIstrazivanja = mutableListOf("")
        igViewModel.getIstrazivanjeByGodina(godina)
        igViewModel.istrazivanje.observe(viewLifecycleOwner, Observer { ist ->
            istrList = ist
                anListViewModel.getMyAnkete()
                anListViewModel.response.observe(viewLifecycleOwner, Observer { listaAnketa->
                    for(i in ist ){
                        var da = true
                        listaAnketa.forEach {
                            if (it.istrazivanje == i.naziv)
                                da = false
                        }
                        if(da && !listaIstrazivanja.contains(i.naziv))
                            listaIstrazivanja.add(i.naziv)
                    }
                })
            val adapter = activity?.applicationContext?.let {
                ArrayAdapter(
                    it,
                    android.R.layout.simple_spinner_item, listaIstrazivanja)
            }
            adapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            istrazivanje.adapter = adapter
        })
    }

    private fun popuniGrupe(istrazivanje: String) {
        grupa = view?.findViewById(R.id.odabirGrupa)!!
        val listaGrupa = mutableListOf("")
        istrList.forEach {
            if(it.naziv == istrazivanje)
                igViewModel.getGroupsByIstrazivanje(it.id)
        }
        igViewModel.grupa.observe(viewLifecycleOwner, Observer { gr ->
            grList = gr
            for(i in gr){
                listaGrupa.add(i.naziv)
            }
            val adapter = activity?.applicationContext?.let {
                ArrayAdapter(
                    it,
                    android.R.layout.simple_spinner_item, listaGrupa)
            }
            adapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            grupa.adapter = adapter
        })

    }

   private fun upisNaAnketu(view: View) {
        val istrazivanje = view.findViewById<Spinner>(R.id.odabirIstrazivanja).selectedItem.toString()
        val grupa = view.findViewById<Spinner>(R.id.odabirGrupa).selectedItem.toString()
        val poruka = "Uspješno ste upisani u\ngrupu $grupa istraživanja $istrazivanje !"
        communicator = activity as Communicator
       var gr: Grupa? = null
       grList.forEach {
            if(grupa == it.naziv)
                gr=it
        }
       var ist : Istrazivanje? = null
       istrList.forEach {
           if(istrazivanje == it.naziv)
               ist = it
       }
       igViewModel.dodajIstrazivanjeIGrupu(ist!!,gr!!)

       grList.forEach {
           if(it.naziv == grupa) {
               igViewModel.upisiUGrupu(it.id)
               igViewModel.getAnketeZaGrupu(it.id)
           }
       }
       igViewModel.ankete.observe(viewLifecycleOwner, Observer { lista->
           lista.forEach {
               anViewModel.zapocniAnketu(it.id)
           }
           igViewModel.upisan.observe(viewLifecycleOwner, Observer {
               if(it){
                   communicator.passDataCon(poruka)
               }
           })
       })

       //popuniIstrazivanja(view.findViewById<Spinner>(R.id.odabirGodina).selectedItem.toString().toInt())
       //popuniGrupe(view.findViewById<Spinner>(R.id.odabirIstrazivanja).selectedItem.toString())
    }

    fun checkForInternet(): Boolean {
        val context = requireContext()
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val network = connectivityManager.activeNetwork ?: return false
            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false
            return when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        } else {
            @Suppress("DEPRECATION") val networkInfo =
                connectivityManager.activeNetworkInfo ?: return false
            @Suppress("DEPRECATION")
            return networkInfo.isConnected
        }
    }
}