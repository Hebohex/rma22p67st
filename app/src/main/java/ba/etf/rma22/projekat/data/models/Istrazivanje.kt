package ba.etf.rma22.projekat.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Istrazivanje(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val naziv: String,
    val godina: Int
)