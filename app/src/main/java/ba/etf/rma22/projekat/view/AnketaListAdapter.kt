package ba.etf.rma22.projekat.view

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.AnketaTaken
import java.text.SimpleDateFormat
import java.util.*
import java.util.Calendar.getInstance

class AnketaListAdapter(
    private var ankete: List<Anketa>,
    private val listener: OnItemClickListener
) : RecyclerView.Adapter<AnketaListAdapter.AnketaViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnketaViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_anketa, parent, false)
        return AnketaViewHolder(view)
    }
    override fun getItemCount(): Int = ankete.size
    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: AnketaViewHolder, position: Int) {
        val cal: Calendar = getInstance()
        val date: Date = cal.time
        holder.anketaTitle.text = ankete[position].naziv
        holder.istrazivanjeTitle.text = ankete[position].istrazivanje
        holder.grupaTitle.text = ankete[position].grupa
        if(ankete[position].datumPocetak.time<date.time && ankete[position].progres == 100){
            holder.status.setImageResource(R.drawable.plava)
            holder.datum.text = "Anketa urađena"
            holder.boja="plava"
        }else if (ankete[position].datumPocetak.time<date.time && ankete[position].datumKraj.time>date.time){
            holder.status.setImageResource(R.drawable.zelena)
            holder.datum.text = "Vrijeme zatvaranja: "+ ankete[position].datumKraj.dateToString()
            holder.boja="zelena"
        }else if (ankete[position].datumPocetak.time>date.time){
            holder.status.setImageResource(R.drawable.zuta)
            holder.datum.text = "Vrijeme aktiviranja: "+ ankete[position].datumPocetak.dateToString()
            holder.boja="zuta"
        }else if (ankete[position].datumKraj<=date){
            holder.status.setImageResource(R.drawable.crvena)
            holder.datum.text = " Anketa zatvorena: "+ ankete[position].datumKraj.dateToString()
            holder.boja="crvena"
        }
        holder.progres.progress = (ankete[position].progres)
    }
    @SuppressLint("NotifyDataSetChanged")
    fun updateAnkete(ankete: List<Anketa>) {
        this.ankete = ankete
        notifyDataSetChanged()
    }
    private fun Date.dateToString(): String {
        //simple date formatter
        val format = "dd.MM.yyyy"
        val dateFormatter = SimpleDateFormat(format, Locale.getDefault())

        //return the formatted date string
        return dateFormatter.format(this)
    }
    inner class AnketaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener{
        val anketaTitle: TextView = itemView.findViewById(R.id.naslovAnkete)
        val istrazivanjeTitle: TextView = itemView.findViewById(R.id.naslovIstrazivanja)
        val grupaTitle: TextView = itemView.findViewById(R.id.naslovGrupe)
        val status: ImageView = itemView.findViewById(R.id.status)
        val progres: ProgressBar = itemView.findViewById(R.id.progresZavrsetka)
        val datum: TextView = itemView.findViewById(R.id.datum)
        var boja: String = ""
        init {
            itemView.setOnClickListener(this)
        }
        override fun onClick(p0: View?) {
            listener.onItemClick(anketaTitle.text.toString(), istrazivanjeTitle.text.toString(), boja)
        }
    }

    interface OnItemClickListener {
        fun onItemClick(anketa: String, istrazivanje: String, status: String)
    }
}