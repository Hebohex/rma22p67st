package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.data.models.Account
import ba.etf.rma22.projekat.data.models.AnketaTaken
import org.json.JSONObject
import org.json.JSONTokener
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

object AccountRepository {
    var acHash = "5747a099-b0e3-4695-b371-4e3ac8f6a095"

    fun postaviHash(acHash: String):Boolean{
        if(acHash.isEmpty())
            return false

        this.acHash = acHash
        return true
    }

    fun getHash():String{
        try {
            val url = URL (ApiConfig.baseURL+"/student/$acHash")
            (url.openConnection() as? HttpURLConnection)?.run {
                BufferedReader(InputStreamReader(inputStream)).use {
                    val response = StringBuffer()

                    var inputLine = it.readLine()
                    while (inputLine != null) {
                        response.append(inputLine)
                        inputLine = it.readLine()
                    }
                    //println("Response : $response")

                    val jsonObject = JSONTokener(response.toString()).nextValue() as JSONObject
                    acHash = jsonObject.getString("acHash")
                }
            }
        }catch (e: Exception){
            println("Greska: $e")
        }
        return acHash
    }

    fun getStudent():Account?{
        try {
            val url = URL (ApiConfig.baseURL+"/student/$acHash")
            (url.openConnection() as? HttpURLConnection)?.run {
                BufferedReader(InputStreamReader(inputStream)).use {
                    val response = StringBuffer()

                    var inputLine = it.readLine()
                    while (inputLine != null) {
                        response.append(inputLine)
                        inputLine = it.readLine()
                    }
                    //println("Response : $response")

                    val jsonObject = JSONTokener(response.toString()).nextValue() as JSONObject
                    val id = jsonObject.getInt("id")
                    val student = jsonObject.getString("student")
                    acHash = jsonObject.getString("acHash")

                    return Account(id,student, acHash)
                }
            }
        }catch (e: Exception){
            println("Greska: $e")
        }
        return null
    }
}