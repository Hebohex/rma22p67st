package ba.etf.rma22.projekat.view

import android.content.Context
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import ba.etf.rma22.projekat.Communicator
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.data.repositories.PitanjeAnketaRepository
import ba.etf.rma22.projekat.viewmodel.AnketaViewModel


class FragmentPitanje(
    private var anketa: Anketa,
    private var status: String,
    private var index: Int
) : Fragment() {

    private lateinit var tekstPitanje : TextView
    private lateinit var listaOdgovori : ListView
    private lateinit var zaustavi : Button
    private lateinit var communicator: Communicator
    private var listaPitanja : List<Pitanje> = listOf()
    private lateinit var anketaViewModel:AnketaViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_pitanje, container, false)

        anketaViewModel = ViewModelProvider(this).get(AnketaViewModel::class.java)

        tekstPitanje = view.findViewById(R.id.tekstPitanja)
        listaOdgovori = view.findViewById(R.id.odgovoriLista)
        zaustavi = view.findViewById(R.id.dugmeZaustavi)
        communicator = activity as Communicator

        if(checkForInternet()) {
            anketaViewModel.odgovorene(anketa.id)
            anketaViewModel.getPitanja(anketa.id)


        anketaViewModel.pitanja.observe(viewLifecycleOwner, Observer {
            listaPitanja = it
            if (listaPitanja.isNotEmpty()) {
                tekstPitanje.text = listaPitanja[index].tekstPitanja

                val adapter: ArrayAdapter<String?> = object : ArrayAdapter<String?>(
                    this.requireActivity(),
                    android.R.layout.simple_list_item_1,
                    listaPitanja[index].opcije
                ) {
                    var previousSelectedItem: View? = null
                    var previous: TextView? = null

                    override fun getView(
                        position: Int,
                        convertView: View?,
                        parent: ViewGroup
                    ): View {
                        val view = super.getView(position, convertView, parent)
                        if (status != "zelena" || PitanjeAnketaRepository.odgovoreni[index].odgovoreno != -1)
                            view.isEnabled = false
                        val textView = view.findViewById<View>(android.R.id.text1) as TextView
                        view.setOnClickListener {
                            if (previousSelectedItem != null) {
                                val textView2 =
                                    previousSelectedItem!!.findViewById<View>(android.R.id.text1) as TextView
                                textView2.setTextColor(Color.BLACK)
                                anketa.progres -= 100 / listaPitanja.size
                            }
                            if (previous != null)
                                anketa.progres -= 100 / listaPitanja.size
                            previous?.setTextColor(Color.BLACK)
                            previousSelectedItem = view
                            textView.setTextColor(Color.parseColor("#0000FF"))
                            listaPitanja[index].odgovoreno = position
                            anketa.progres = anketa.progres + 100 / listaPitanja.size
                            if (anketa.progres > 100)
                                anketa.progres = 100
                        }
                        if (listaPitanja[index].odgovoreno == position) {
                            previous = textView
                            textView.setTextColor(Color.parseColor("#0000FF"))
                        }
                        return view
                    }
                }
                listaOdgovori.adapter = adapter
            }
        })
        }else{
                anketaViewModel.pitanjaBaze()
                anketaViewModel.pitanjaBaza.observe(viewLifecycleOwner, Observer { baza->
                    val lis = mutableListOf<Pitanje>()
                    baza.forEach {
                        if(it.pitanjeAnketa == anketa.id)
                            lis.add(it)
                    }
                    listaPitanja = lis
                    if(listaPitanja.isNotEmpty()) {
                        tekstPitanje.text = listaPitanja[index].tekstPitanja

                        val adapter: ArrayAdapter<String?> = object : ArrayAdapter<String?>(
                            this.requireActivity(),
                            android.R.layout.simple_list_item_1,
                            listaPitanja[index].opcije
                        ) {
                            var previous: TextView? = null

                            override fun getView(
                                position: Int,
                                convertView: View?,
                                parent: ViewGroup
                            ): View {
                                val view = super.getView(position, convertView, parent)
                                val textView = view.findViewById<View>(android.R.id.text1) as TextView
                                view.isEnabled = false
                                if (listaPitanja[index].odgovoreno == position) {
                                    previous = textView
                                    textView.setTextColor(Color.parseColor("#0000FF"))
                                }
                                return view
                            }
                        }
                        listaOdgovori.adapter = adapter
                    }else{
                        Handler(Looper.getMainLooper()).postDelayed({
                            Toast.makeText(context, "Molim, poveži se na internet", Toast.LENGTH_LONG).show()
                            tekstPitanje.text = "Nije moguće učitati pitanja"
                        },3000)
                    }
                })
            }

        zaustavi.setOnClickListener {
            if(status=="zelena" && checkForInternet()){
                anketaViewModel.getPoceteAnkete()
                anketaViewModel.pocete.observe(viewLifecycleOwner, androidx.lifecycle.Observer { poceta->
                    var id = -1
                    poceta.forEach {
                        if(anketa.id == it.AnketumId) {
                            id = it.id
                        }
                    }
                    if(id!=-1){
                        PitanjeAnketaRepository.pitanja.forEach {
                            anketaViewModel.postaviOdgovorAnketa(id,it.id, it.odgovoreno)
                        }
                        anketaViewModel.azurirajPitanja(PitanjeAnketaRepository.pitanja)

                        communicator = activity as Communicator
                        communicator.vratiNaPocetnu()
                    }
                })
            }else {
                communicator = activity as Communicator
                communicator.vratiNaPocetnu()
            }
        }

        return view
    }

    override fun onPause() {
        super.onPause()
        // pamti odgovore na pitanja
        if(!listaPitanja.isNullOrEmpty()) {
            val lista = mutableListOf<Pitanje>()
            PitanjeAnketaRepository.pitanja.forEach { odg ->
                if (odg.id == listaPitanja[index].id)
                    lista.add(listaPitanja[index])
                else
                    lista.add(odg)
            }
            if (lista.isEmpty())
                PitanjeAnketaRepository.pitanja = listaPitanja as MutableList<Pitanje>
            else
                PitanjeAnketaRepository.pitanja = lista
        }//
    }

    companion object {
        fun newInstance(anketa: Anketa,status: String,index: Int) = FragmentPitanje(anketa,status,index)
    }

    fun checkForInternet(): Boolean {
        val context :Context = requireContext()
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val network = connectivityManager.activeNetwork ?: return false
            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false
            return when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        } else {
            @Suppress("DEPRECATION") val networkInfo =
                connectivityManager.activeNetworkInfo ?: return false
            @Suppress("DEPRECATION")
            return networkInfo.isConnected
        }
    }
}