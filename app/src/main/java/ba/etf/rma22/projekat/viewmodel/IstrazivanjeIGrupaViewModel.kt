package ba.etf.rma22.projekat.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ba.etf.rma22.projekat.data.database.AnketaDao
import ba.etf.rma22.projekat.data.database.AnketaDatabase
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.models.Istrazivanje
import ba.etf.rma22.projekat.data.repositories.AnketaRepository
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeIGrupaRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class IstrazivanjeIGrupaViewModel(application: Application): AndroidViewModel(application) {

    var istrazivanje: MutableLiveData<List<Istrazivanje>> = MutableLiveData()
    var grupa: MutableLiveData<List<Grupa>> = MutableLiveData()
    var upisan: MutableLiveData<Boolean> = MutableLiveData()
    var ankete: MutableLiveData<List<Anketa>> = MutableLiveData()
    val anketaDao: AnketaDao = AnketaDatabase.getDatabase(
        application
    ).anketaDao()

    fun dodajIstrazivanjeIGrupu(istrazivanje: Istrazivanje, grupa: Grupa){
        GlobalScope.launch(Dispatchers.IO) {
            anketaDao.dodajIstrazivanje(istrazivanje)
            anketaDao.dodajGrupu(grupa)
        }
    }

    fun getIstrazivanjeByGodina(godina: Int) {
        GlobalScope.launch(Dispatchers.IO) {
            istrazivanje.postValue(IstrazivanjeIGrupaRepository.getIstrazivanjeByGodina(godina))
            grupa.postValue(listOf())
        }
    }

    fun getGroupsByIstrazivanje(istrazivanje: Int) {
        GlobalScope.launch(Dispatchers.IO) {
            grupa.postValue(IstrazivanjeIGrupaRepository.getGrupeZaIstrazivanje(istrazivanje)-IstrazivanjeIGrupaRepository.getUpisaneGrupe())
        }
    }

    fun upisiUGrupu(grupa: Int) {
        GlobalScope.launch(Dispatchers.IO) {
            upisan.postValue(IstrazivanjeIGrupaRepository.upisiUGrupu(grupa))
        }
    }

    fun getAnketeZaGrupu(idGrupe: Int){
        GlobalScope.launch(Dispatchers.IO) {
            ankete.postValue(AnketaRepository.getAnketeZaGrupu(idGrupe))
        }
    }
}