package ba.etf.rma22.projekat

import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.UtilTestClass
import ba.etf.rma22.projekat.UtilTestClass.Companion.atPosition
import ba.etf.rma22.projekat.UtilTestClass.Companion.firstNotVisited
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.repositories.AnketaRepository
import io.mockk.every
import io.mockk.mockkClass
import io.mockk.mockkObject
import org.hamcrest.CoreMatchers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.Math.round
import java.util.*
import kotlin.collections.ArrayList

@RunWith(AndroidJUnit4::class)
class RecyclerViewTest {
    @get:Rule
    val mainLayout = ActivityScenarioRule<MainActivity>(MainActivity::class.java)

    @Before
    fun setUp() {
        var cal: Calendar = Calendar.getInstance()
        cal.set(2022,3,10)
        var cal1: Calendar = Calendar.getInstance()
        cal1.set(4021,6,10)
        var mojeAnkete = ArrayList(listOf(Anketa("Crvena Anketa", "RMA", cal.time, cal.time, null, 4, "RMA Grupa 1", 0.0f),
                Anketa("Plava Anketa", "RMA", cal.time, cal1.time, cal.time, 4, "RMA Grupa 1", 1.0f),
                Anketa("Zuta Anketa", "RMA", cal1.time, cal1.time, null, 4, "RMA Grupa 1", 0.0f),
                Anketa("Zelena Anketa", "RMA", cal.time, cal1.time, null, 4, "RMA Grupa 1", 0.0f)))
        mockkObject(AnketaRepository)
        every {
            AnketaRepository.getMyAnkete()
        } returns mojeAnkete
    }

    @Test
    fun recyclerViewCheckAllTypes(){
        Espresso.onView(ViewMatchers.withId(R.id.filterAnketa)).perform(ViewActions.click())
        Espresso.onData(
                CoreMatchers.allOf(
                        CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
                        CoreMatchers.`is`("Sve ankete")
                )
        ).perform(ViewActions.click())
        val anketePrije = AnketaRepository.getAll()
        Espresso.onView(ViewMatchers.withId(R.id.listaAnketa))
                .check(UtilTestClass.hasItemCount(anketePrije.size))
        var posjeceni:MutableList<Int> = mutableListOf()
        Espresso.onView(withId(R.id.listaAnketa))
                .perform(RecyclerViewActions.scrollTo<RecyclerView.ViewHolder>(firstNotVisited(posjeceni,ViewMatchers.hasDescendant(UtilTestClass.withDrawable(R.drawable.crvena)))));
        Espresso.onView(withId(R.id.listaAnketa))
                .perform(RecyclerViewActions.scrollTo<RecyclerView.ViewHolder>(firstNotVisited(posjeceni,ViewMatchers.hasDescendant(UtilTestClass.withDrawable(R.drawable.zelena)))));
        Espresso.onView(withId(R.id.listaAnketa))
                .perform(RecyclerViewActions.scrollTo<RecyclerView.ViewHolder>(firstNotVisited(posjeceni,ViewMatchers.hasDescendant(UtilTestClass.withDrawable(R.drawable.plava)))));
        Espresso.onView(withId(R.id.listaAnketa))
                .perform(RecyclerViewActions.scrollTo<RecyclerView.ViewHolder>(firstNotVisited(posjeceni,ViewMatchers.hasDescendant(UtilTestClass.withDrawable(R.drawable.zuta)))));

    }

    @Test
    fun recyclerViewCheckColors(){

        Espresso.onView(ViewMatchers.withId(R.id.filterAnketa)).perform(ViewActions.click())
        Espresso.onData(
                CoreMatchers.allOf(
                        CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
                        CoreMatchers.`is`("Sve moje ankete")
                )
        ).perform(ViewActions.click())
        val ankete = AnketaRepository.getMyAnkete()
        Espresso.onView(ViewMatchers.withId(R.id.listaAnketa))
                .check(UtilTestClass.hasItemCount(4))
        Espresso.onView(withId(R.id.listaAnketa))
                .check(ViewAssertions.matches(atPosition(0, ViewMatchers.hasDescendant(UtilTestClass.withDrawable(R.drawable.crvena)))));
        Espresso.onView(withId(R.id.listaAnketa))
                .check(ViewAssertions.matches(atPosition(1, ViewMatchers.hasDescendant(UtilTestClass.withDrawable(R.drawable.plava)))));
      /*  Espresso.onView(withId(R.id.listaAnketa))
                .check(ViewAssertions.matches(atPosition(1, ViewMatchers.hasDescendant(ViewMatchers.withText(ankete[1].progres.toString())))));
       */
        Espresso.onView(withId(R.id.listaAnketa))
                .check(ViewAssertions.matches(atPosition(2, ViewMatchers.hasDescendant(UtilTestClass.withDrawable(R.drawable.zelena)))));
        Espresso.onView(withId(R.id.listaAnketa))
                .check(ViewAssertions.matches(atPosition(3, ViewMatchers.hasDescendant(UtilTestClass.withDrawable(R.drawable.zuta)))));
    }

    @Test
    fun recyclerProgressTest(){
        Espresso.onView(ViewMatchers.withId(R.id.filterAnketa)).perform(ViewActions.click())
        Espresso.onData(
            CoreMatchers.allOf(
                CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
                CoreMatchers.`is`("Sve moje ankete")
            )
        ).perform(ViewActions.click())
        val ankete = AnketaRepository.getMyAnkete()
        Espresso.onView(ViewMatchers.withId(R.id.listaAnketa))
            .check(UtilTestClass.hasItemCount(4))
            .check(ViewAssertions.matches(atPosition(0, ViewMatchers.hasDescendant(UtilTestClass.withProgress(0)))));
        Espresso.onView(withId(R.id.listaAnketa))
            .check(ViewAssertions.matches(atPosition(1, ViewMatchers.hasDescendant(UtilTestClass.withProgress(100)))));
        Espresso.onView(withId(R.id.listaAnketa))
            .check(ViewAssertions.matches(atPosition(2, ViewMatchers.hasDescendant(UtilTestClass.withProgress(0)))));
        Espresso.onView(withId(R.id.listaAnketa))
            .check(ViewAssertions.matches(atPosition(3, ViewMatchers.hasDescendant(UtilTestClass.withProgress(0)))));


    }
}
